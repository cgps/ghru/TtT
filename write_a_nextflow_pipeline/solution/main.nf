// remove trailing slash 
input_dir = params.input_dir - ~/\/$/ 
// setup path to reads by combining the input_dir and fastq_pattern params
fastqs = input_dir + '/' + params.fastq_pattern
// setup output_dir
output_dir = params.output_dir

// get databases from params
ecoli_db = file(params.ecoli_db)
refseq_db = file(params.refseq_db)

// make a channel consisting of the fastq pair prefix and the pair of fastq files
Channel
    .fromFilePairs( fastqs )
    .ifEmpty { error "Cannot find any reads matching: ${fastqs}" }
    .set { reads }

// sketch the reads
process sketch_reads{
    tag { pair_id }

    input:
    set pair_id, file(read_pair) from reads

    output:
    set pair_id, file("${pair_id}.msh") into sketches_for_ecoli, sketches_for_refseq

    script:
    """
    # alternative command: cat $read_pair[0] $read_pair[1] | mash sketch -m 3 -o $pair_id - 
    cat ${read_pair[0]} ${read_pair[1]} > combined.fastq.gz
    mash sketch -m 3 -o ${pair_id} combined.fastq.gz
    """
}
// compare read sketches with E.coli database
process compare_with_ecoli{
    tag { pair_id }
    publishDir output_dir, mode: 'copy'

    input:
    set pair_id, file(sample_sketch) from sketches_for_ecoli
    file('ecoli_db.msh') from ecoli_db

    output:
    file("${pair_id}_vs_ecoli.tsv") into ecoli_results

    script:
    """
    mash dist -C ecoli_db.msh ${sample_sketch} | sort -t'\t' -gk3 | head -10 > ${pair_id}_vs_ecoli.tsv
    """
}

// compare read sketches with Refseq database
process compare_with_refseq{
    tag { pair_id }
    publishDir output_dir, mode: 'copy'

    input:
    set pair_id, file(sample_sketch) from sketches_for_refseq
    file('refseq_db.msh') from refseq_db

    output:
    file("${pair_id}_vs_refseq.tsv") into refseq_results

    script:
    """
    mash dist -C refseq_db.msh ${sample_sketch} | sort -t'\t' -gk3 | head -10 > ${pair_id}_vs_refseq.tsv
    """
}
