// remove trailing slash 
input_dir = params.input_dir - ~/\/$/ 
// setup path to reads by combining the input_dir and fastq_pattern params
fastqs = input_dir + '/' + params.fastq_pattern
// setup output_dir
output_dir = params.output_dir

// get databases from params
ecoli_db = file(params.ecoli_db)
refseq_db = file(params.refseq_db)

// make a channel consisting of the fastq pair prefix and the pair of fastq files
Channel
    .fromFilePairs( <READS VARIABLE> )
    .ifEmpty { error "Cannot find any reads matching: ${<READS VARIABLE>}" }
    .set { <READ CHANNEL NAME>}

// sketch the reads
process sketch_reads{
    tag { pair_id }

    input:
    set ........ from <READ CHANNEL NAME>

    output:
    set ........ into <CHANNEL 1>, <CHANNEL 2>

    script:
    """
    # combine the read 1 and read 2 fastqs
    cat <READ 1> <READ 2> > combined.fastq.gz
    # sketch the combined reads and remove low abundance kmers (they must occur at least 3 times)
    mash sketch -m 3 -o <OUTPUT NAME> combined.fastq.gz
    """
}
// compare read sketches with E.coli database
process compare_with_ecoli{
    tag { pair_id }
    publishDir output_dir, mode: 'copy'

    input:
    set ........  from <CHANNEL 1>
    file('ecoli_db.msh') from ecoli_db

    output:
    file ........

    script:
    """
    # find distances to the E.coli database, sort by distance and return the first 10 lines
    mash dist -C <ECOLI DB> <SAMPLE SKETCH> | sort -t'\t' -gk3 | head -10 > ${pair_id}_vs_ecoli.tsv
    """
}

// compare read sketches with Refseq database
process compare_with_refseq{
    tag { pair_id }
    publishDir output_dir, mode: 'copy'

    input:
    set ........  from <CHANNEL 2>
    file('refseq_db.msh') from refseq_db

    output:
    file .......

    script:
    """
    # find distances to the refseq database, sort by distance and return the first 10 lines
    mash dist -C <REFSEQ DB> <SAMPLE SKETCH> | sort -t'\t' -gk3 | head -10 > ${pair_id}_vs_refseq.tsv
    """
}
